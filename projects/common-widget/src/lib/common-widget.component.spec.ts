import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonWidgetComponent } from './common-widget.component';

describe('CommonWidgetComponent', () => {
  let component: CommonWidgetComponent;
  let fixture: ComponentFixture<CommonWidgetComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CommonWidgetComponent]
    });
    fixture = TestBed.createComponent(CommonWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
