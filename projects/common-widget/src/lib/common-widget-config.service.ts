import { InjectionToken } from '@angular/core';
import {CommonWidgetConfig} from "./common-widget.module";

export const commonWidgetConfig = new InjectionToken<CommonWidgetConfig>("CommonWidgetConfig");
