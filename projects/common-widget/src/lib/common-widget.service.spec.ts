import { TestBed } from '@angular/core/testing';

import { CommonWidgetService } from './common-widget.service';

describe('CommonWidgetService', () => {
  let service: CommonWidgetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CommonWidgetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
