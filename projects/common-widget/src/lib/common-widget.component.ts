import {Component, OnInit} from '@angular/core';
import {CommonWidgetService} from "./common-widget.service";

@Component({
  selector: 'lib-common-widget',
  template: `
    <p>
      common-widget works!
    </p>
    <ul>
      <li *ngFor="let item of data$ | async">
        {{item.label}} - {{item.value}}
      </li>
    </ul>
  `
})
export class CommonWidgetComponent implements OnInit {

  data$ = this.commonWidgetService.data$;

  constructor(
    private commonWidgetService: CommonWidgetService
  ) {
  }

  ngOnInit(): void {
    this.commonWidgetService.getData().subscribe();
  }
}
