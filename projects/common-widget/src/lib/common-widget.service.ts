import {Inject, Injectable} from '@angular/core';
import {BehaviorSubject, Observable, tap} from "rxjs";
import {Data} from "./data";
import {HttpClient} from "@angular/common/http";
import {commonWidgetConfig} from "./common-widget-config.service";
import {CommonWidgetConfig} from "./common-widget.module";

@Injectable({
  providedIn: 'root'
})
export class CommonWidgetService {

  data = new BehaviorSubject<Data[]>([
    {
      label: "label 1",
      value: "value 1"
    }
  ]);

  get data$(): Observable<Data[]> {
    return this.data.asObservable();
  }

  constructor(
    private http: HttpClient,
    @Inject(commonWidgetConfig) private config: CommonWidgetConfig
  ) {

  }

  getData() {
    return this.http.get<Data[]>(this.config.endpoint).pipe(tap(data => this.data.next(data)));
    // return this.http.get<Data[]>(this.config.endpoint).pipe(tap(data => this.data.next(data)));
  }

}
