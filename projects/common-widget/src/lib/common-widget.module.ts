import {InjectionToken, ModuleWithProviders, NgModule} from '@angular/core';
import { CommonWidgetComponent } from './common-widget.component';
import {AsyncPipe, NgForOf} from "@angular/common";
import {CommonWidgetService} from "./common-widget.service";
import {HttpClientModule} from "@angular/common/http";
import {commonWidgetConfig} from "./common-widget-config.service";

export interface CommonWidgetConfig {
  endpoint: string;
}

@NgModule({
  declarations: [
    CommonWidgetComponent
  ],
  imports: [
    NgForOf,
    AsyncPipe,
    HttpClientModule
  ],
  exports: [
    CommonWidgetComponent
  ]
})
export class CommonWidgetModule {
  static forRoot(config: CommonWidgetConfig): ModuleWithProviders<CommonWidgetModule>{
    return {
      ngModule: CommonWidgetModule,
      providers: [
        {
          provide: commonWidgetConfig,
          useValue: config
        }
      ]
    }
  }
}
