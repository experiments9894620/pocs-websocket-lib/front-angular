import {Component, OnInit} from '@angular/core';
import {ArticleService} from "./article.service";
import {Observable} from "rxjs";
import {Article} from "./article";
import {Panier} from "../../../panier-widget/src/lib/panier";
import {PanierService} from "../../../panier-widget/src/lib/panier.service";
import {PanierWidgetService} from "../../../panier-widget/src/lib/panier-widget.service";

@Component({
  selector: 'app-root',
  template: `
    <h1>{{title}}</h1>
    <select [(ngModel)]="token">
      <option *ngFor="let user of users" [value]="user.token">{{user.name}}</option>
    </select>
    <button (click)="updateToken()">Connect</button>
    <ul *ngFor="let article of (articleListe$ | async)">
      <li> {{ article.label }} pour {{ article.prix }} €
        <button (click)="addArticle(article)" [disabled]="isConnected()"> add</button>
      </li>
    </ul>
    <lib-panier-widget></lib-panier-widget>
  `
})
export class AppComponent implements OnInit {
  title = 'project A';
  token!: string;

  users = [
    {
      name: "toto",
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoidG90byJ9LCJpYXQiOjE1MTYyMzkwMjJ9.Lw4KHTGq53yJsj2mw241G-ddHwSM1J75cDEAEgiulOg'
    },
    {
      name: "tata",
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoidGF0YSJ9LCJpYXQiOjE1MTYyMzkwMjJ9.5z2tnqpyM4DBfie0d5ylyEZYIBscMUZdLj9OoYxUHIM'
    },
    {
      name: "titi",
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoidGl0aSJ9LCJpYXQiOjE1MTYyMzkwMjJ9.CHDLGOUAN7m_r2K94M3tMhKRSpIXNrZPsRQlBVgWO_E'
    }
  ]

  articleListe$: Observable<Article[]> = this.articleService.articleList$

  isConnected() {
    return this.panierService.getToken() === undefined
  }

  constructor(
    private articleService: ArticleService,
    private panierService: PanierService,
    private panierWsService: PanierWidgetService
  ) {
  }

  ngOnInit() {
    this.articleService.getArticles().subscribe();
  }

  addArticle(article: Article) {
    this.panierService.addArticle({
      name: article.label,
      prix: article.prix,
      quantite: 1
    }).subscribe();
  }

  updateToken() {
    this.panierService.setToken(this.token);
    this.panierWsService.connectToWS()
  }

}
