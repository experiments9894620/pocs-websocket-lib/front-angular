import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable, tap} from "rxjs";
import {Article} from "./article";

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  articleListe = new BehaviorSubject<Article[]>([]);

  get articleList$(): Observable<Article[]> {
    return this.articleListe.asObservable();
  }

  constructor(
    private http: HttpClient
  ) { }

  getArticles() {
    return this.http.get<Article[]>("/json-server/catalogue-a").pipe(tap(articleListe => this.articleListe.next(articleListe)));
  }
}
