import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {CommonWidgetModule} from "common-widget";
import {FormsModule} from "@angular/forms";
import {PanierWidgetModule} from "../../../panier-widget/src/lib/panier-widget.module";

@NgModule({
  declarations: [
    AppComponent
  ],
    imports: [
        BrowserModule,
        CommonWidgetModule.forRoot({
            endpoint: "http://localhost:3000/project-a"
        }),
        PanierWidgetModule.forRoot({
            endpoint: "http://localhost:3000/project-a"
        }),
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
