export interface Article {
  designation: string;
  poidsEnKilo: number;
  prixAuKilo: number;
}
