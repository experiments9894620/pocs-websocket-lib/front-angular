/*
 * Public API Surface of panier-widget
 */

export * from './lib/panier.service';
export * from './lib/panier-widget.service';
export * from './lib/panier-widget.component';
export * from './lib/panier-widget.module';
