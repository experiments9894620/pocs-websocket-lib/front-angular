import {Injectable} from '@angular/core';
import {io, Socket} from "socket.io-client";
import {Panier} from "./panier";
import {PanierService} from "./panier.service";
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PanierWidgetService {

  private paniers: BehaviorSubject<Panier | null> = new BehaviorSubject<Panier | null>(null);

  get panier$() {
    return this.paniers.asObservable();
  }

  get wsConnected() {
    return this.socket ? this.socket.connected : false;
  }

  socket: Socket = io(`ws://localhost:3101/paniers`, {
    auth: (cb) => {
      cb({token: this.panierService.getToken()})
    },
    autoConnect: false
  })

  constructor(
    private panierService: PanierService
  ) {
  }

  connectToWS() {
    if (this.wsConnected) {
      this.socket.disconnect();
    }
    this.socket.connect();
    this.socket.on('panier', (panierListe: Panier) => this.paniers.next(panierListe));
  }
}
