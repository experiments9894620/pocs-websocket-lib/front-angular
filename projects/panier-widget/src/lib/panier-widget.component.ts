import {Component} from '@angular/core';
import {PanierWidgetService} from "./panier-widget.service";
import {ArticlePanier, Panier} from "./panier";
import {PanierService} from "./panier.service";
import {Observable} from "rxjs";

@Component({
  selector: 'lib-panier-widget',
  template: `
    <div *ngIf=" (panier$ | async) as panier ">
      <p *ngFor="let articlePanier of panier?.articleListe">
        {{ articlePanier.name }} pour {{ articlePanier.prix }} (x{{ articlePanier.quantite }}) <button (click)="delete(articlePanier)"> remove </button>
      </p>
      <p>Total : {{panier?.prixTotal}}</p>
    </div>
  `,
  styles: [
  ]
})
export class PanierWidgetComponent {

  panier$ = this.panierWidgetService.panier$;

  constructor(
    private panierWidgetService: PanierWidgetService,
    private panierService: PanierService
  ) {
  }

  delete(article: ArticlePanier) {
    return this.panierService.delete(article.id!).subscribe();
  }
}
