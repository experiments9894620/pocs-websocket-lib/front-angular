import { InjectionToken } from '@angular/core';
import {PanierWidgetConfig} from "./panier-widget.module";

export const panierWidgetConfig = new InjectionToken<PanierWidgetConfig>("PanierWidgetConfig");
