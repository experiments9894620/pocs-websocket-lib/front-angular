export interface Panier {
  articleListe: ArticlePanier[];
  prixTotal: number;
}

export interface  ArticlePanier {
  id?: number;
  name: string;
  prix: number;
  quantite: number;
}
