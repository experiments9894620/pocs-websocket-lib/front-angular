import {Injectable} from '@angular/core';
import {ArticlePanier, Panier} from "./panier";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
@Injectable({
  providedIn: 'root'
})
export class PanierService {

  token!: string;

  getToken(): string {
    return this.token;
  }

  setToken(token: string) {
    this.token = token;
  }

  constructor(
    private http: HttpClient
  ) {
  }

  addArticle(article: ArticlePanier) {
    return this.http.post<Panier>("/nestjs/api/v1/articles", article);
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`/nestjs/api/v1/articles/${id}`);
  }
}
