import {ModuleWithProviders, NgModule} from '@angular/core';
import { PanierWidgetComponent } from './panier-widget.component';
import {panierWidgetConfig} from "./panier-widget-config.service";
import {AsyncPipe, NgForOf, NgIf} from "@angular/common";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {BasicAuthInterceptor} from "./basic-auth.interceptor";

export interface PanierWidgetConfig {
  endpoint: string;
}

@NgModule({
  declarations: [
    PanierWidgetComponent
  ],
  exports: [
    PanierWidgetComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
  ],
  imports: [
    NgForOf,
    NgIf,
    AsyncPipe,
  ],
})
export class PanierWidgetModule {
  static forRoot(config: PanierWidgetConfig): ModuleWithProviders<PanierWidgetModule>{
    return {
      ngModule: PanierWidgetModule,
      providers: [
        {
          provide: panierWidgetConfig,
          useValue: config
        }
      ]
    }
  }

}
