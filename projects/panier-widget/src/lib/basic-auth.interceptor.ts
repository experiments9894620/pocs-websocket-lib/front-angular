import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {catchError, Observable, throwError} from 'rxjs';
import {PanierService} from "./panier.service";

@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {

  constructor(
    private panierService: PanierService
  ) {
  }

  intercept(
    // eslint-disable-next-line  @typescript-eslint/no-explicit-any
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<string>> {
    console.log(req.url)
    const headers = new HttpHeaders({
      Authorization:
        `Bearer ` + this.panierService.getToken(),
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Accept: 'application/json',
    });
    const authreq = req.clone({ headers });

    // permet de récupérer la réponse de la requête
    return next.handle(authreq).pipe(
      catchError((error: HttpErrorResponse) => {
        return throwError(() => {
          return error;
        });
      })
    );
  }
}
